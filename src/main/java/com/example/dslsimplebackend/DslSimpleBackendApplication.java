package com.example.dslsimplebackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DslSimpleBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(DslSimpleBackendApplication.class, args);
	}

}
