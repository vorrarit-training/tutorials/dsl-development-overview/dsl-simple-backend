package com.example.dslsimplebackend.service;

import com.example.dslsimplebackend.model.Account;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.text.NumberFormat;

@Service
public class AccountService {

    private static Logger log = LoggerFactory.getLogger(AccountService.class);

    public Account getAccountInfo(String accountNo) {
        return new Account(accountNo, randomString(6), randomString(6), randomLoanAmount());
    }

    private String randomString(int length) {
        String rndString = "";
        String text = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        for (int i=0; i<length; i++) {
            Double num = (Math.random() * 100);
            Integer charIdx = num.intValue() % 25;
//            log.debug(num.toString() + " " + charIdx.toString());
            rndString += text.substring(charIdx, charIdx + 1);
        }
        return rndString;
    }

    private Double randomLoanAmount() {
        Double num = (Math.random() * 1000);
        DecimalFormat df = new DecimalFormat("0.00");
        return Double.parseDouble(df.format(num));
    }
}
