package com.example.dslsimplebackend.model;

public class Account {

    private String accountNo = "0000";
    private String firstName = "Default First Name";
    private String lastName = "Default Last Name";
    private Double loanAmount = 50.0;

    public Account() {
    }

    public Account(String accountNo, String firstName, String lastName, Double loanAmount) {
        this.accountNo = accountNo;
        this.firstName = firstName;
        this.lastName = lastName;
        this.loanAmount = loanAmount;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Double getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(Double loanAmount) {
        this.loanAmount = loanAmount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Account account = (Account) o;

        if (!accountNo.equals(account.accountNo)) return false;
        if (!firstName.equals(account.firstName)) return false;
        if (!lastName.equals(account.lastName)) return false;
        return loanAmount.equals(account.loanAmount);
    }

    @Override
    public int hashCode() {
        int result = accountNo.hashCode();
        result = 31 * result + firstName.hashCode();
        result = 31 * result + lastName.hashCode();
        result = 31 * result + loanAmount.hashCode();
        return result;
    }
}
