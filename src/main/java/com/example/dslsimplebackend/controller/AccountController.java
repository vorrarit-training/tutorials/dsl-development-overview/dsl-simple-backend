package com.example.dslsimplebackend.controller;

import com.example.dslsimplebackend.model.Account;
import com.example.dslsimplebackend.service.AccountService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/account")
public class AccountController {

    @Autowired
    private AccountService accountService;

    @ApiOperation(value = "getInfo", notes = "get account balance by using accountNo")
    @GetMapping("/info/{accountNo}/{delay}")
    public Account getInfo(@ApiParam("account number")  @PathVariable String accountNo, @ApiParam("delay in ms") @PathVariable Integer delay) {
        try {
            Thread.sleep(delay);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return accountService.getAccountInfo(accountNo);
    }
}
