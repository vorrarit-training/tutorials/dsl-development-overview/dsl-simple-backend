Jenkins

Manage Jenkins > Configure System > Global properties

DEFAULT_REGISTRY
registry url e.g. registry.digitalocean.com/myregistry

MVN_SOURCE_VOLUME
.m2 folder as recognized by docker mvn agent

KUBE_CREDENTIAL
A name refer to PKCS#12 certificate credential as set in Manage Jenkins > Manage Credential e.g. kube-cert 

KUBE_API
kubernetes api url e.g. https://192.168.64.6:8443

KUBE_CONTEXT

KUBE_CLUSTER
